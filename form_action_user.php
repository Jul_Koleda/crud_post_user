<?php
$host     = 'localhost';
$user     = 'root';
$password = '';
$database = 'joins_db';
$link     = mysqli_connect($host, $user, $password, $database);
if (!$link) {
    die('Connect Error: ' . mysqli_connect_error());
} else {
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $user_name   = htmlspecialchars($_POST['user_name']);
    $user_surname    = htmlspecialchars($_POST['user_surname']);
    $user_birthdate    = htmlspecialchars($_POST['user_birthdate']);
    $user_gender    = htmlspecialchars($_POST['user_gender']);
    $user_marital    = htmlspecialchars($_POST['user_marital']);
    $user_biography = htmlspecialchars($_POST['user_biography']);

//    echo $user_title . "<br />" . $user_text . "<br />" . $user_date . "<br />" . $user_user_id . "<br />";
    $sql = "INSERT INTO `user` (`id`, `user_name`, `user_surname`, `user_birthdate`, `user_gender`,`user_marital`,`user_biography`,) VALUES ";
    $sql .= "(NULL , ";
    $sql .= "'" . "$user_name" . "'";
    $sql .= ", " . "'" . "$user_surname" . "'";
    $sql .= ", " . "'" . "$user_birthdate" . "'";
    $sql .= ", " . "'" . "$user_gender" . "'";
    $sql .= ", " . "'" . "$user_marital" . "'";
    $sql .= ", " . "$user_biography";
    $sql .= ")";

    if ($link->query($sql) === true) {
//        echo "New record created successfully";
    } else {
//        echo "Error: " . $sql . "<br>" . $link->error;
    }
//    echo $_SERVER['REQUEST_URL'];
    header('location: http://'. $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}
$result = $link->query('SELECT * FROM `user`');

$link->close();


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <link href="style/actions.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>user</title>
</head>
<body>


<table>
    <tr>
        <th>#</th>
        <th>name</th>
        <th>surname</th>
        <th>birth date</th>
        <th>gender</th>
        <th>marital</th>
        <th>bio</th>
        <th width="8%"><a href="user.php" id="add">add</a></th>
    </tr>
    <?php while ($row = mysqli_fetch_array($result)) { ?>

        <tr>
            <td><?= $row['id'] ?></td>
            <td><?= $row['user_name'] ?></td>
            <td><?= $row['user_surname'] ?></td>
            <td><?= $row['user_birthdate'] ?></td>
            <td><?= $row['user_gender'] ?></td>
            <td><?= $row['user_marital'] ?></td>
            <td><?= $row['user_biography'] ?></td>
            <td width="8%"><a href="/crud/CRUD_update_user.php?edit=<?= $row["id"] ?>" id="edit">edit </a></td>
            <td width="8%"><a href="/crud/CRUD_del_and_save_user.php?del=<?= $row["id"] ?>" id="del">del</a></td>
        </tr>

    <?php }
    ?>
</table>
</body>
</html>

