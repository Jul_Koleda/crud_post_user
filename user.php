<!DOCTYPE html>
<html lang="en">

<head>
    <link href="style/style_user.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>user</title>
</head>
<body>
<h2>No Symfony No Bootstrap</h2>
<form name="form_user" method="post" action="form_action_user.php">
    <div class="main">
        <div>
            <p><b>Your Name</b><br>
                <label for="user_name">
                    <input name='user_name' id="user_name" type="text" size="20" auto-validate pattern="[a-Z]*"
                           maxlength="50"
                           placeholder="what is your name?">
                </label>
            </p>
        </div>

        <div>
            <p><b>Your Surname</b><br>
                <label for="user_surname">
                    <input type="text" id="user_surname" name='user_surname' size="20" auto-validate pattern="[a-Z]*"
                           maxlength="50"
                           placeholder="what is your surname?"<?php if (isset($_GET['edit'])) {
                        echo $_POST['user_surname'];
                    } ?>" />
                </label>
            </p>
        </div>

        <div class="radio_gender">
            <label for="male">
                <input type="radio" id="male" name="radio_gender" value="0"
                       checked=""/>
                Male
            </label>
            <label for="female">
                <input type="radio" id="female" name="radio_gender" value="1"/>
                Female
            </label>
        </div>

        <div class="user_birthday">
            <p><b>Birthday</b><br>
                <label for="user_birthdate">
                    <input type="date" name='user_birthdate' id="user_birthdate">
                </label>
            </p>
        </div>

        <div class="user_marital">
            <label for="user_marital">
                Marital?
                <input type="checkbox" id="user_marital" name="user_marital" value="1">
            </label>
        </div>

        <div class="biography">
            <p>Biography<Br>
                <label for="user_biography">
            <input name="user_biography" id="user_biography" cols="40" rows="3" auto-validate pattern="[a-Z]*"
                      maxlength="2000"
                      placeholder="bio...">
                </label></p>
        </div>

        <div class="submit">
            <input type="submit" name="submit" value="Send" id="submit"/>
        </div>

    </div>
</form>
</body>
</html>

