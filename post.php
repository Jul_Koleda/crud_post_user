<?php
$host     = 'localhost';
$user     = 'root';
$password = '';
$database = 'joins_db';
$link     = mysqli_connect($host, $user, $password, $database);
if (!$link) {
    die('Connect Error: ' . mysqli_connect_error());
} else {
//    echo "All ok";
}

$result = mysqli_query($link, 'SELECT user_name,id FROM `joins_db`.`user`');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <link href="style/style_post.css" rel="stylesheet">
    <meta charset="UTF-8">
    <title>Post</title>
</head>
<body>
<h2>No Symfony No Bootstrap</h2>
<form name="form_user" method="post" action="form_action_post.php">
    <div class="main">
        <div>
            <p><b>Title Post</b><br>
                <label for="post_title">
                    <input name='post_title' type="text" size="50" id="post_title" auto-validate pattern="[a-Z]*"
                           maxlength="200" placeholder="Title" value="<?php if (isset($_GET['edit'])) {
                        echo $getROW['fn'];
                    } ?>"/>
                </label>
            </p>
        </div>

        <div>
            <p>Your Post<Br>
                <label for="post_text">
                <textarea name="post_text" cols="40" rows="3" id="post_text" auto-validate pattern="[a-Z]*"
                          maxlength="2000" placeholder="post"></textarea>
                </label></p>
        </div>

        <div class="post_date">
            <p><b>Date Post</b><br>
                <label for="postdate">
                    <input type="date" name='post_date' id="post_date">
                </label>
            </p>
        </div>

        <div class="post_user_id">
            <p><b>Select User</b><br>
                <select name="post_user_id" id="post_user_id">
                    <?php while ($row = mysqli_fetch_array($result)) { ?>
                        <option value="<?= $row['id'] ?>"><?= $row['user_name'] ?></option>
                    <?php } ?>
                </select>
            </p>
        </div>

        <div class="submit">
            <input type="submit" name="submit" value="Send" id="submit"/>
        </div>
    </div>

</form>

</body>
</html>
