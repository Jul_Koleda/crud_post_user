<?php
include_once '../connect.php';
$res    = mysqli_query($link, 'SELECT user_name,id FROM `joins_db`.`user`');
$postId = (int)$_GET['edit'];
if ($postId === 0) {
    header('Location: /form_action_post.php');
}

// if form submitted
if (isset($_POST['post_title']) && $_POST['post_text']) {
    $post_title   = mysqli_real_escape_string($link, $_POST['post_title']);
    $post_text    = mysqli_real_escape_string($link, $_POST['post_text']);
    $post_date    = mysqli_real_escape_string($link, $_POST['post_date']);
    $post_user_id = mysqli_real_escape_string($link, $_POST['post_user_id']);

    // checking empty fields
    if (empty($post_title) || empty($post_text) || empty($post_date) || empty($post_user_id)) {
        if (empty($post_title)) {
            echo "field is empty.";
        }

        if (empty($post_text)) {
            echo "field is empty.";
        }

        if (empty($post_date)) {
            echo "field is empty";
        }

        if (empty($post_user_id)) {
            echo "field is empty";
        }
    } else {
        //updating the table
        $result = mysqli_query($link,
            "UPDATE `post` SET post_title='$post_title',post_text='$post_text',post_date='$post_date',post_user_id='$post_user_id' WHERE id=$postId");

        //redirectig to the display page. In our case, it is index.php
        header("Location: /form_action_post.php");
    }
}

//selecting data associated with this particular id
$result = mysqli_query($link, "SELECT * FROM post WHERE id = " . $postId);
$post   = mysqli_fetch_assoc($result);

?>
<html>
<head>
    <link href="/style/style_post.css" rel="stylesheet">
    <title>Edit Data</title>
</head>

<body>
<h2>No Symfony No Bootstrap</h2>
<form name="form1" method="post" action="CRUD_UPDATE.php">
    <div class="main">
        <div>
            <p><b>Title Post</b><br>
                <label for="post_title">
                    <input name='post_title' type="text" size="50" id="post_title" auto-validate pattern="[a-Z]*"
                           maxlength="200" placeholder="Title" value="<?= $post['post_title'] ?>">
                </label>
            </p>
        </div>

        <div>
            <p>Your Post<Br>
                <label for="post_text">
                    <input name="post_text" type="text" cols="40" rows="3" id="post_text" auto-validate pattern="[a-Z]*"
                           maxlength="2000" placeholder="post" value="<?= $post['post_text'] ?>">
                </label></p>
        </div>

        <div class="post_date">
            <p><b>Date Post</b><br>
                <label for="postdate">
                    <input type="date" name='post_date' id="post_date" value="<?= $post['post_date'] ?>">
                </label>
            </p>
        </div>

        <div class="post_user_id">
            <p><b>Select User</b><br>
                <select name="post_user_id" id="post_user_id">
                    <?php while ($row = mysqli_fetch_array($res)) { ?>
                        <option value="<?= $row['id'] ?>"><?= $row['user_name'] ?></option>
                    <?php } ?>
                </select>
            </p>
        </div>

        <div class="submit">
            <input type="submit" name="submit" value="Send" id="submit"/>
        </div>
    </div>
</form>
</body>
</html>