<?php
include_once '../connect.php';
//$res    = mysqli_query($link, 'SELECT user_name,id FROM `joins_db`.`user`');
$userId = (int)$_GET['edit'];
if ($userId === 0) {
    header('Location: /form_action_user.php');
}

// if form submitted
if (isset($_POST['user_name']) && $_POST['user_surname'] && $_POST['user_birthaday']&& $_POST['user_gender']&& $_POST['user_marital'] && $_POST['user_biography']) {
    $user_name      = mysqli_real_escape_string($link, $_POST['user_name']);
    $user_surname   = mysqli_real_escape_string($link, $_POST['user_surname']);
    $user_birthday  = mysqli_real_escape_string($link, $_POST['user_birthaday']);
    $user_gender    = mysqli_real_escape_string($link, $_POST['user_gender']);
    $user_marital   = mysqli_real_escape_string($link, $_POST['user_marital']);
    $user_biography = mysqli_real_escape_string($link, $_POST['user_biography']);
}
if (isset($_POST['user_name']) && $_POST['user_surname'] && $_POST['user_birthaday']&& $_POST['user_gender']&& $_POST['user_marital'] && $_POST['user_biography']) {

    // checking empty fields
    if (empty($user_name) || empty($user_surname) || empty($user_birthday) || empty($user_biography)) {
        if (empty($user_name)) {
            echo "field is empty.";
        }

        if (empty($user_surname)) {
            echo "field is empty.";
        }

        if (empty($user_birthday)) {
            echo "field is empty";
        }

        if (empty($user_biography)) {
            echo "field is empty";
        }
    } else {
        //updating the table
        $result = mysqli_query($link,
            "UPDATE `user` SET user_name='$user_name', user_surname='$user_surname',user_birthday='$user_birthday',user_gender='$user_gender',user_marital='$user_marital',user_biography='$user_biography' WHERE id=$userId");

        //redirectig to the display page. In our case, it is index.php
        header("Location: /form_action_user.php");
    }
}

//selecting data associated with this particular id
$result = mysqli_query($link, "SELECT * FROM user WHERE id = " . $userId);
$user   = mysqli_fetch_assoc($result);

?>
<html>
<head>
    <link href="/style/style_user.css" rel="stylesheet">
    <title>Edit Data</title>
</head>

<body>
<form name="form_user" method="post" action="/form_action_user.php">
    <div class="main">
        <div>
            <p><b>Your Name</b><br>
                <label for="user_name">
                    <input name='user_name' id="user_name" type="text" size="20" pattern="[a-Z]*"
                           maxlength="50"
                           placeholder="what is your name?" value="<?= $user['user_name'] ?>">
                </label>
            </p>
        </div>

        <div>
            <p><b>Your Surname</b><br>
                <label for="user_surname">
                    <input type="text" id="user_surname" name='user_surname' size="20" pattern="[a-Z]*"
                           maxlength="50"
                           placeholder="what is your surname?" value="<?= $user['user_surname'] ?>">
                </label>
            </p>
        </div>

        <div class="radio_gender">
            <label for="male">
                <input type="radio" id="male" name="radio_gender" value="0"
                       checked=""/>
                Male
            </label>
            <label for="female">
                <input type="radio" id="female" name="radio_gender" value="1"/>
                Female
            </label>
        </div>

        <div class="user_birthday">
            <p><b>Birthday</b><br>
                <label for="user_birthdate">
                    <input type="date" name='user_birthdate' id="user_birthdate" value="<?= $user['user_birthday'] ?>">
                </label>
            </p>
        </div>

        <div class="user_marital">
            <label for="user_marital">
                Marital?
                <input type="checkbox" id="user_marital" name="user_marital" value="1">
            </label>
        </div>

        <div class="biography">
            <p>Biography<Br>
                <label for="user_biography">
                    <input name="user_biography" id="user_biography"  pattern="[a-Z]*"
                           maxlength="2000"
                           placeholder="bio..." value="<?= $user['user_biography'] ?>">
                </label></p>
        </div>

        <div class="submit">
            <input type="submit" name="submit" value="Send" id="submit"/>
        </div>

    </div>
</form>
</body>
</html>