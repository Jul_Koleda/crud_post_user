<?php
$host     = 'localhost';
$user     = 'root';
$password = '';
$database = 'joins_db';
$link     = mysqli_connect($host, $user, $password, $database);
if (!$link) {
    die('Connect Error: ' . mysqli_connect_error());
} else {
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $post_title   = htmlspecialchars($_POST['post_title']);
    $post_text    = htmlspecialchars($_POST['post_text']);
    $post_date    = htmlspecialchars($_POST['post_date']);
    $post_user_id = htmlspecialchars($_POST['post_user_id']);

//    echo $post_title . "<br />" . $post_text . "<br />" . $post_date . "<br />" . $post_user_id . "<br />";
    $sql = "INSERT INTO `post` (`id`, `post_title`, `post_text`, `post_date`, `post_user_id`) VALUES ";
    $sql .= "(NULL , ";
    $sql .= "'" . "$post_title" . "'";
    $sql .= ", " . "'" . "$post_text" . "'";
    $sql .= ", " . "'" . "$post_date" . "'";
    $sql .= ", " . "$post_user_id";
    $sql .= ")";

    if ($link->query($sql) === true) {
//        echo "New record created successfully";
    } else {
//        echo "Error: " . $sql . "<br>" . $link->error;
    }
//    echo $_SERVER['REQUEST_URL'];
    header('location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
}
$result = $link->query('SELECT * FROM `post`');

$link->close();
print_r($_GET);
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <link href="style/actions.css" rel="stylesheet">
        <meta charset="UTF-8">
        <title>user</title>
    </head>
    <body>
    <table>
        <tr>
            <th width="10%">#</th>
            <th>title</th>
            <th>text</th>
            <th>date</th>
            <th>user</th>
            <th width="8%"><a href="post.php" id="add">add</a></th>


        </tr>
        <?php while ($row = mysqli_fetch_array($result)) { ?>

            <tr>
                <td><?= $row['id'] ?></td>
                <td><?= $row['post_title'] ?></td>
                <td><?= $row['post_text'] ?></td>
                <td><?= $row['post_date'] ?></td>
                <td><?= $row['post_user_id'] ?></td>

                <td width="8%"><a href="/crud/CRUD_update_post.php?edit=<?= $row["id"] ?>" id="edit">edit </a></td>
                <td width="8%">
                    <a href="/crud/CRUD_del_and_save_post.php?del=<?= $row["id"] ?>" id="del" onclick="return confirm('Are you sure?');">del</a>
                </td>


            </tr>
        <?php }
        ?>

    </table>
    </body>
    </html>

<?php
if (isset($_GET['del'])) {
    $sql = $link->prepare("DELETE FROM `post` WHERE id=" . $_GET['del']);
    $sql->bind_param("i", $_GET['del']);
    $sql->execute();
    header("Location: index.php");
}